import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

misp_service = ['redis', 'httpd', 'misp-workers', 'misp-modules', 'mariadb']


def test_misp_is_installed(host):
    for misp_srv in misp_service:
        misp = host.service(misp_srv)
        assert misp.is_enabled
