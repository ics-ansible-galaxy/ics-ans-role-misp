# ics-ans-role-misp

Ansible role to install misp.

## Role Variables

```yaml
# change password
misp_mysql_pw: password
misp_org: ess
misp_email_addr: account@example.com
misp_db_host: localhost
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-misp
```

## License

BSD 2-clause
